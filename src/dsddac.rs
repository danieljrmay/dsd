//use alsa::card;
use alsa::direct::pcm::MmapPlayback;
use alsa::pcm::Access;
use alsa::pcm::Format;
use alsa::pcm::HwParams;
use alsa::pcm::State;
//use alsa::pcm::IO;
use alsa::pcm::PCM;
use alsa::Direction;
use alsa::ValueOr;

use super::dsf::DSF;

pub const FREQUENCY_PCM_44_1K: u32 = 44100;
pub const FREQUENCY_PCM_48K: u32 = 48000;
pub const FREQUENCY_PCM_88_2K: u32 = 88200;
pub const FREQUENCY_PCM_96K: u32 = 96000;
pub const FREQUENCY_PCM_176_4K: u32 = 176400;
pub const FREQUENCY_PCM_192K: u32 = 192000;
pub const FREQUENCY_PCM_352_8K: u32 = 352800;
pub const FREQUENCY_PCM_384K: u32 = 384000;
pub const FREQUENCY_PCM_705_6K: u32 = 705600;
pub const FREQUENCY_PCM_768K: u32 = 768000;

pub const FREQUENCY_DSD_64: u32 = 2822400;
pub const FREQUENCY_DSD_128: u32 = 5644800;
pub const FREQUENCY_DSD_256: u32 = 11289600;
pub const FREQUENCY_DSD_512: u32 = 22579200;

fn pcm_frequency(dsd_frequency: u32) -> u32 {
    match dsd_frequency {
        FREQUENCY_DSD_64 => FREQUENCY_PCM_88_2K,
        FREQUENCY_DSD_128 => FREQUENCY_PCM_176_4K,
        FREQUENCY_DSD_256 => FREQUENCY_PCM_352_8K,
        FREQUENCY_DSD_512 => FREQUENCY_PCM_352_8K,
        _ => panic!(
            "Unrecognised DSD sampling frequency of {}Hz.",
            dsd_frequency
        ),
    }
}

pub enum DSDDACErr {
    PCMErr,
}

pub struct DSDDAC {
    access: Access,
    channels: u32,
    format: Format,
    pcm: PCM,
    rate: u32,
}
impl DSDDAC {
    pub fn new(alsa_name: &str) -> Result<DSDDAC, DSDDACErr> {
        match PCM::new(alsa_name, Direction::Playback, false) {
            Ok(pcm) => {
                let access = Access::RWInterleaved;
                let channels: u32 = 2;
                let format = Format::DSDU32BE;
                let rate = pcm_frequency(FREQUENCY_DSD_64);

                let dac = DSDDAC {
                    access,
                    channels,
                    format,
                    pcm,
                    rate,
                };
                dac.init();

                Ok(dac)
            }
            Err(_) => Err(DSDDACErr::PCMErr),
        }
    }

    pub fn new_from_dsf(alsa_name: &str, dsf: &DSF) -> Result<DSDDAC, DSDDACErr> {
        match PCM::new(alsa_name, Direction::Playback, false) {
            Ok(pcm) => {
                let access = Access::MMapInterleaved;
                let channels: u32 = dsf.metadata.fmt_chunk.channel_num;
                let format = Format::DSDU32BE;
                let rate = pcm_frequency(dsf.metadata.fmt_chunk.sampling_frequency);

                let dac = DSDDAC {
                    access,
                    channels,
                    format,
                    pcm,
                    rate,
                };

                dac.init();

                Ok(dac)
            }
            Err(_) => Err(DSDDACErr::PCMErr),
        }
    }

    pub fn print(&self) {
        println!("\nDSD DAC:");
        println!("Access = {:?}", self.access);
        println!("Channels = {}", self.channels);
        println!("Format = {:?}", self.format);
        println!("PCM frequency = {}", self.rate);

        match self.pcm.info() {
            Ok(info) => {
                println!("\nInfo:");
                println!("Card = {}", info.get_card());
                println!("Device = {}", info.get_device());
                println!("Subdevice = {}", info.get_subdevice());
                println!("Id = {:?}", info.get_id());
                println!("Name = {:?}", info.get_name());
                println!("Subdevice name = {:?}", info.get_name());
                println!("Stream = {:?}", info.get_stream());
            }
            Err(_) => {
                println!("\nFailed to get info struct.");
            }
        }

        match self.pcm.hw_params_current() {
            Ok(hwp) => {
                println!("\nHwParams:");
                println!("Channels = {}", hwp.get_channels().unwrap());
                println!("Rate = {}", hwp.get_rate().unwrap());
                println!("Format = {:?}", hwp.get_format().unwrap());
                println!("Access = {:?}", hwp.get_access().unwrap());
                println!("Period size = {:?} frames", hwp.get_period_size().unwrap());
                println!("Buffer size = {:?} frames", hwp.get_buffer_size().unwrap());
            }
            Err(_) => {
                println!("\nFailed to get current HwParams struct.");
            }
        }

        match self.pcm.sw_params_current() {
            Ok(swp) => {
                println!("\nSwParams:");
                println!("Avail Min = {} frames", swp.get_avail_min().unwrap());
                println!("Boundary = {} frames", swp.get_boundary().unwrap());
                println!(
                    "Start threshold = {:?} frames",
                    swp.get_start_threshold().unwrap()
                );
                println!(
                    "Stop threshold = {:?} frames",
                    swp.get_stop_threshold().unwrap()
                );
                println!("Tstamp mode = {:?}", swp.get_tstamp_mode().unwrap());
            }
            Err(_) => {
                println!("\nFailed to get current SwParams struct.");
            }
        }
    }

    fn init(&self) {
        self.set_parameters(self.channels, self.rate, self.format, self.access);
    }

    fn set_parameters(&self, channels: u32, rate: u32, format: Format, access: Access) {
        let hwp = HwParams::any(&self.pcm).unwrap();
        hwp.set_channels(channels).unwrap();
        hwp.set_rate(rate, ValueOr::Nearest).unwrap();
        hwp.set_format(format).unwrap();
        hwp.set_access(access).unwrap();

        self.pcm.hw_params(&hwp).unwrap();

        let swp = self.pcm.sw_params_current().unwrap();
        swp.set_start_threshold(hwp.get_buffer_size().unwrap() - hwp.get_period_size().unwrap())
            .unwrap();
        self.pcm.sw_params(&swp).unwrap();
    }

    fn dsf_matches_current_parameters(&self, dsf: &DSF) -> bool {
        if self.channels == dsf.metadata.fmt_chunk.channel_num
            && self.rate == pcm_frequency(dsf.metadata.fmt_chunk.sampling_frequency)
        {
            true
        } else {
            false
        }
    }

    pub fn play(&self, dsf: &mut DSF) {
        if self.dsf_matches_current_parameters(dsf) {
            println!("\nDSF file matches current DSD DAC paramters.");
        } else {
            println!("\nDSF file does not matche current DSD DAC paramters.");
            return;
        }

        println!("Getting MmapPlayback...");
        let mut mmpb: MmapPlayback<u32> = self.pcm.direct_mmap_playback().unwrap();

        let mut unfinished: bool = true;

        while unfinished {
            if mmpb.avail() > 0 {
                if mmpb.write(&mut dsf.sample_data) == 0 {
                    println!("\nDraining PCM.");
                    self.pcm.drain().unwrap();
                    unfinished = false;
                    println!("Finished");
                }
            }

            match mmpb.status().state() {
                State::Running => print!("\r{}", dsf.format_elapsed_of_total_duration()),
                State::Prepared => {
                    println!("Prepared. Will now start output stream.");
                    self.pcm.start();
                }
                State::XRun => {
                    println!("Underrun in audio output stream.");
                    self.pcm.prepare();
                    unfinished = false;
                }
                State::Suspended => {
                    println!("Resuming audio output stream.");
                    self.pcm.resume();
                    unfinished = false;
                }
                State::Draining => {
                    println!("PCM state is drainging and about to leave loop.");
                    unfinished = false;
                }
                _ => {
                    println!("Unexpected PCM state {:?}", mmpb.status().state());
                    unfinished = false;
                }
            }
        }
        return;
    }
}
