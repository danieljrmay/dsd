use dsd::dsddac::DSDDAC;
use dsd::dsf::DSF;
use std::env;
use std::path::Path;

fn main() {
    println!("DSF Play");

    let clargs = CLArgs::new();
    if clargs.verbose {
        clargs.print();
    }

    if clargs.help {
        clargs.print_usage();
        std::process::exit(0);
    }

    let input_path = Path::new(&clargs.input_filename);
    println!("\nFile = {}", input_path.display());

    match DSF::new_from_path(input_path) {
        Ok(mut dsf) => {
            println!("\nDSF file details:{}", dsf);
            match DSDDAC::new_from_dsf("iec958:CARD=D50,DEV=0", &dsf) {
                Ok(dac) => {
                    dac.print();
                    //dsf.sample_data.print_dsf_frame();
                    dac.play(&mut dsf);
                }
                Err(e) => println!("Error getting DAC."),
            }
        }
        Err(_err) => println!("Error parsing DSF file."),
    }
}

struct CLArgs {
    help: bool,
    input_filename: String,
    verbose: bool,
}
impl CLArgs {
    pub fn new() -> CLArgs {
        let mut help = false;
        let mut input_filename = String::from("index.xhtml");
        let mut verbose = false;

        let mut args: Vec<String> = env::args().collect();
        let mut i = 0;

        while i < args.len() {
            match args[i].as_ref() {
                "--help" | "-h" => {
                    help = true;
                }
                "--verbose" | "-v" => {
                    verbose = true;
                }
                _ => {
                    if i == args.len() - 1 {
                        input_filename = args.remove(i);
                    }
                }
            }

            i += 1;
        }

        CLArgs {
            help,
            input_filename,
            verbose,
        }
    }

    fn print(&self) {
        println!("Help: {}", self.help);
        println!("Input filename: {}", self.input_filename);
        println!("Verbose: {}", self.verbose);
        println!("");
    }

    fn print_usage(&self) {
        println!("Usage: dsf-play [OPTIONS] INPUT_DSF_FILE");
        println!("Options:");
        println!(" -h, --help\t\t\tshow this message and exit");
        println!(" -v, --verbose\t\t\tprint messages");
        println!("\nFor more information see <https://gitlab.com/danieljrmay/dsd>")
    }
}
