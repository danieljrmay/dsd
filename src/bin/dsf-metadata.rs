extern crate clap;
extern crate dsf;

use clap::{App, Arg, Values};
use std::path::Path;

fn main() {
    let clargs = App::new("DSF Metadata")
        .version("0.1.0")
        .author("Daniel J. R. May <daniel@danieljrmay.com>")
        .about("Display DSF file metadata.")
        .arg(
            Arg::with_name("paths")
                .required(true)
                .min_values(1)
                .help("DSF file path(s)"),
        )
        .get_matches();

    println!("DSF Metadata");

    let paths: Values = match clargs.values_of("paths") {
        Some(p) => p,
        None => unreachable!(
            "The command line argument parsing crate 'clap' should ensure that paths \
             have been provided or a syntax error message is printed for the user."
        ),
    };

    for path_as_str in paths {
        let path = Path::new(path_as_str);

        let dsf_file = match dsf::DsfFile::open(path) {
            Ok(dsf_file) => dsf_file,
            Err(error) => {
                println!("Error: {}", error.to_string());
                std::process::exit(1);
            }
        };

        println!("\nFile: {}\n\n{}\n", path_as_str, dsf_file);
    }
}
