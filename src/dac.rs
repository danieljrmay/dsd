use alsa::card;
use alsa::pcm::Access;
use alsa::pcm::Format;
use alsa::pcm::HwParams;
use alsa::pcm::State;
use alsa::pcm::IO;
use alsa::pcm::PCM;
use alsa::Direction;
use alsa::ValueOr;

pub const CHANNELS_STEREO: u32 = 2;

pub const PCM_RATE_44_1K: u32 = 44100;
pub const PCM_RATE_48K: u32 = 48000;
pub const PCM_RATE_88_2K: u32 = 88200;
pub const PCM_RATE_96K: u32 = 96000;
pub const PCM_RATE_176_4K: u32 = 176400;
pub const PCM_RATE_192K: u32 = 192000;
pub const PCM_RATE_352_8K: u32 = 352800;
pub const PCM_RATE_384K: u32 = 384000;
pub const PCM_RATE_705_6K: u32 = 705600;
pub const PCM_RATE_768K: u32 = 768000;

pub const DSD_RATE_2_82M: u32 = 88200; // DSD64
pub const DSD_RATE_5_64M: u32 = 176400; // DSD128
pub const DSD_RATE_11_28M: u32 = 352800; // DSD256
pub const DSD_RATE_22_56M: u32 = 705600; // DSD512

pub struct Encoding {
    access: Access,
    channels: u32,
    format: Format,
    rate: u32,
}
impl Encoding {
    pub fn new(access: Access, channels: u32, format: Format, rate: u32) -> Encoding {
        Encoding {
            access,
            channels,
            format,
            rate,
        }
    }

    pub fn new_pcm(channels: u32, rate: u32) -> Encoding {
        Encoding::new(Access::RWInterleaved, channels, Format::S32LE, rate)
    }

    pub fn new_dsd(channels: u32, rate: u32) -> Encoding {
        Encoding::new(Access::RWInterleaved, channels, Format::DSDU32BE, rate)
    }

    pub fn print(&self) {
        println!("\nEncoding:");
        println!("Access = {:?}", self.access);
        println!("Channels = {}", self.channels);
        println!("Format = {:?}", self.format);
        println!("Rate = {}", self.rate);
    }
}

pub const BUFFER_SIZE: usize = 1024;

pub struct DAC {
    pcm: PCM,
    encoding: Encoding,
}
impl DAC {
    pub fn new(alsa_name: &str) -> Result<DAC, DACErr> {
        match PCM::new(alsa_name, Direction::Playback, false) {
            Ok(pcm) => {
                let encoding = Encoding::new_dsd(CHANNELS_STEREO, DSD_RATE_2_82M);
                let dac = DAC { pcm, encoding };
                dac.init();

                Ok(dac)
            }
            Err(_) => Err(DACErr::PCMErr),
        }
    }

    fn init(&self) {
        self.set_parameters(
            self.encoding.channels,
            self.encoding.rate,
            self.encoding.format,
            self.encoding.access,
        );
    }

    //-> Result<HwParams, DACErr>
    fn set_parameters(&self, channels: u32, rate: u32, format: Format, access: Access) {
        let hwp = HwParams::any(&self.pcm).unwrap();
        hwp.set_channels(channels).unwrap();
        hwp.set_rate(rate, ValueOr::Nearest).unwrap();
        hwp.set_format(format).unwrap();
        hwp.set_access(access).unwrap();

        self.pcm.hw_params(&hwp).unwrap();

        let swp = self.pcm.sw_params_current().unwrap();
        swp.set_start_threshold(hwp.get_buffer_size().unwrap() - hwp.get_period_size().unwrap())
            .unwrap();
        self.pcm.sw_params(&swp).unwrap();
    }

    pub fn play_silence(&self, duration: u32) {
        match self.encoding.format {
            Format::S32LE => {}
            Format::DSDU32BE => {}
            _ => {
                println!("Unkown format in Encoding.");
                return;
            }
        }

        let io = self.pcm.io_i32().unwrap();

        let buffer: [i32; BUFFER_SIZE] = [0; BUFFER_SIZE];

        let iterations =
            (self.encoding.channels * self.encoding.rate * duration) / (BUFFER_SIZE as u32);

        println!("\nPlaying:");
        for _ in 0..iterations {
            io.writei(&buffer);
        }

        if self.pcm.state() != State::Running {
            println!("Starting PCM as not started.");
            self.pcm.start().unwrap();
        }

        println!("Draining PCM.");
        self.pcm.drain().unwrap();

        println!("Finished");
    }

    #[allow(overflowing_literals)]
    pub fn play_dsd_silence(&self, duration: u32) {
        println!("\nAttempting to play DSD silence.");

        println!("Getting io...");
        let io = self.pcm.io_u32().unwrap();

        println!("Creating buffer...");
        let mut buffer: [u32; 4096] = [0; 4096];

        println!("Generating iterations...");
        let iterations = self.encoding.rate * duration / 1024;

        println!("\nPlaying DSD silence:");
        for _ in 0..iterations {
            io.writei(&buffer);
        }

        if self.pcm.state() != State::Running {
            println!("Starting PCM as not started.");
            self.pcm.start().unwrap();
        }

        println!("Draining PCM.");
        self.pcm.drain().unwrap();

        println!("Finished");
    }

    pub fn print(&self) {
        self.encoding.print();
        self.print_info();
        self.print_hw_parameters();
        self.print_sw_parameters();
    }

    pub fn print_info(&self) {
        match self.pcm.info() {
            Ok(info) => {
                println!("\nInfo:");
                println!("Card = {}", info.get_card());
                println!("Device = {}", info.get_device());
                println!("Subdevice = {}", info.get_subdevice());
                println!("Id = {:?}", info.get_id());
                println!("Name = {:?}", info.get_name());
                println!("Subdevice name = {:?}", info.get_name());
                println!("Stream = {:?}", info.get_stream());
            }
            Err(_) => {
                println!("\nFailed to get info struct.");
            }
        }
    }

    pub fn print_hw_parameters(&self) {
        match self.pcm.hw_params_current() {
            Ok(hwp) => {
                println!("\nHwParams:");
                println!("Channels = {}", hwp.get_channels().unwrap());
                println!("Rate = {}", hwp.get_rate().unwrap());
                println!("Format = {:?}", hwp.get_format().unwrap());
                println!("Access = {:?}", hwp.get_access().unwrap());
                println!("Period size = {:?} frames", hwp.get_period_size().unwrap());
                println!("Buffer size = {:?} frames", hwp.get_buffer_size().unwrap());
            }
            Err(_) => {
                println!("\nFailed to get current HwParams struct.");
            }
        }
    }

    pub fn print_sw_parameters(&self) {
        match self.pcm.sw_params_current() {
            Ok(swp) => {
                println!("\nSwParams:");
                println!("Avail Min = {} frames", swp.get_avail_min().unwrap());
                println!("Boundary = {} frames", swp.get_boundary().unwrap());
                println!(
                    "Start threshold = {:?} frames",
                    swp.get_start_threshold().unwrap()
                );
                println!(
                    "Stop threshold = {:?} frames",
                    swp.get_stop_threshold().unwrap()
                );
                println!("Tstamp mode = {:?}", swp.get_tstamp_mode().unwrap());
            }
            Err(_) => {
                println!("\nFailed to get current SwParams struct.");
            }
        }
    }
}

pub enum DACErr {
    PCMErr,
}

pub fn playback() {
    // Open default device for playback
    //let pcm = PCM::new("default", Direction::Playback, false).unwrap();
    //let pcm = PCM::new("\"D50\"", Direction::Playback, false).unwrap();
    //let pcm = PCM::new("iec958", Direction::Playback, false).unwrap();
    let pcm = PCM::new("iec958:CARD=D50,DEV=0", Direction::Playback, false).unwrap();

    // Get current hardware params
    //let chwp = pcm.hw_params_current().unwrap();

    let sampling_rate: u32 = 48000;

    // Set hardware parameters to: Mono 16 bit 44.1kHz
    let hwp = HwParams::any(&pcm).unwrap();
    hwp.set_rate_resample(false).unwrap();
    hwp.set_channels(2).unwrap();
    hwp.set_rate(sampling_rate, ValueOr::Nearest).unwrap();
    hwp.set_format(Format::s32()).unwrap();
    hwp.set_access(Access::RWInterleaved).unwrap();
    pcm.hw_params(&hwp).unwrap();

    let hwp = pcm.hw_params_current().unwrap();
    println!("\nHwParams:");
    println!("Resample = {}", hwp.get_rate_resample().unwrap());
    println!("Channels = {}", hwp.get_channels().unwrap());
    println!("Rate = {}", hwp.get_rate().unwrap());
    println!("Format = {:?}", hwp.get_format().unwrap());
    println!("Access = {:?}", hwp.get_access().unwrap());
    println!("Period size = {:?} frames", hwp.get_period_size().unwrap());
    println!("Buffer size = {:?} frames", hwp.get_buffer_size().unwrap());

    // Make sure we don't start the stream too early

    let swp = pcm.sw_params_current().unwrap();
    swp.set_start_threshold(hwp.get_buffer_size().unwrap() - hwp.get_period_size().unwrap())
        .unwrap();
    pcm.sw_params(&swp).unwrap();

    // Make a sine wave
    let mut buf = [0i32; 1024];
    for (i, a) in buf.iter_mut().enumerate() {
        *a = ((i as f32 * 2.0 * std::f32::consts::PI / 128.0).sin() * 8192.0) as i32
    }

    let my_io = pcm.io_i32().unwrap();

    // Play it back for 2 seconds
    for _ in 0..2 * 44100 / 1024 {
        assert_eq!(my_io.writei(&buf[..]).unwrap(), 512);
    }

    // In case the buffer was larger than 2 seconds, start the stream manually
    if pcm.state() != State::Running {
        pcm.start().unwrap();
    }

    // Wait for the stream to finish playback
    pcm.drain().unwrap();
}

// rough stuff
pub fn print_dacs() {
    let iter = card::Iter::new();

    for result in iter {
        match result {
            Ok(card) => {
                let card_index = card.get_index();
                let card_name = card.get_name().unwrap();
                let card_longname = card.get_longname().unwrap();

                println!("Card[{:?}]={:?}={:?}", card_index, card_name, card_longname);

                let pcm_result = PCM::new("hw:0,0", Direction::Playback, false);
                match pcm_result {
                    Ok(pcm) => {
                        println!("Got pcm");
                        //let hw_params = pcm.hw_params_current().unwrap();
                        //println!("Current hw_params: channels={:?}", hw_params.get_channels());
                    }
                    Err(pcm_err) => {
                        println!("PCM error {}", pcm_err);
                    }
                }
            }
            Err(err) => {
                println!("Error in card iteration: {}", err);
            }
        }
    }
}
