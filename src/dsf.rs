use std::fmt;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufWriter;
use std::io::Read;
use std::io::SeekFrom;
use std::io::Write;
use std::path::Path;
use std::u64;

fn u64_from_byte_buffer(buffer: &[u8], index: usize) -> u64 {
    let mut byte_array: [u8; 8] = [0; 8];
    byte_array.copy_from_slice(&buffer[index..index + 8]);

    u64::from_le_bytes(byte_array)
}

fn u32_from_byte_buffer(buffer: &[u8], index: usize) -> u32 {
    let mut byte_array: [u8; 4] = [0; 4];
    byte_array.copy_from_slice(&buffer[index..index + 4]);

    u32::from_le_bytes(byte_array)
}

/*
DSF stands for "DSD Stream File". They should have the `*.dsf` file extension.

A DSF file is made of 4 chunks:

DSD chunk
fmt chunk
data chunk
metadata chunk

See DSF File Format Specification version 1.01 by Sony Corporation.
 */

pub struct DSF {
    pub metadata: DSFMetadata,
    pub sample_data: DSFSampleData,
}
impl DSF {
    pub fn new_from_path(path: &Path) -> Result<DSF, DSFError> {
        let (metadata, file) = DSFMetadata::new_from_path_get_file(path)?;
        let sample_data = DSFSampleData::new(file, &metadata)?;

        Ok(DSF::new(metadata, sample_data))
    }

    fn new(metadata: DSFMetadata, sample_data: DSFSampleData) -> DSF {
        DSF {
            metadata,
            sample_data,
        }
    }

    pub fn format_elapsed_of_total_duration(&self) -> String {
        let sampling_frequency = self.metadata.fmt_chunk.sampling_frequency as u64;
        let total_samples = self.metadata.fmt_chunk.sample_count;
        let total_seconds = total_samples / sampling_frequency;

        let duration_hours = total_seconds / 3600;
        let duration_minutes = (total_seconds / 60) - duration_hours * 60;
        let duration_seconds = total_seconds - (duration_hours * 3600 + duration_minutes * 60);

        let elapsed_seconds = (self.sample_data.get_fraction() * total_seconds as f64) as u64;
        let elapsed_hours = elapsed_seconds / 3600;
        let elapsed_minutes = (elapsed_seconds / 60) - elapsed_hours * 60;
        let elapsed_seconds = elapsed_seconds - (elapsed_hours * 3600 + elapsed_minutes * 60);

        format!(
            "{:02}:{:02}:{:02}/{:02}:{:02}:{:02}",
            elapsed_hours,
            elapsed_minutes,
            elapsed_seconds,
            duration_hours,
            duration_minutes,
            duration_seconds
        )
    }
}
impl fmt::Display for DSF {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "DSF metadata:\n\n{}\n\nDSF sample data:\n{}\n",
            self.metadata, self.sample_data
        )
    }
}

pub struct DSFMetadata {
    pub dsd_chunk: DSDChunk,
    pub fmt_chunk: FmtChunk,
    pub data_chunk: DataChunk,
    // TODO add idv3v2
}
impl DSFMetadata {
    pub fn new_from_path(path: &Path) -> Result<DSFMetadata, DSFError> {
        let (dsf_metadata, _file) = DSFMetadata::new_from_path_get_file(path)?;

        Ok(dsf_metadata)
    }

    fn new_from_path_get_file(path: &Path) -> Result<(DSFMetadata, File), DSFError> {
        let mut file = File::open(path)?;

        let mut dsd_chunk_buffer: [u8; 28] = [0; 28];
        file.read_exact(&mut dsd_chunk_buffer)?;
        let dsd_chunk = DSDChunk::new_from_buffer(dsd_chunk_buffer)?;

        let mut fmt_chunk_buffer: [u8; 52] = [0; 52];
        file.read_exact(&mut fmt_chunk_buffer)?;
        let fmt_chunk = FmtChunk::new_from_buffer(fmt_chunk_buffer)?;

        let mut data_chunk_buffer: [u8; 12] = [0; 12];
        file.read_exact(&mut data_chunk_buffer)?;
        let data_chunk = DataChunk::new_from_buffer(data_chunk_buffer)?;

        Ok((DSFMetadata::new(dsd_chunk, fmt_chunk, data_chunk), file))
    }

    fn new(dsd_chunk: DSDChunk, fmt_chunk: FmtChunk, data_chunk: DataChunk) -> DSFMetadata {
        DSFMetadata {
            dsd_chunk,
            fmt_chunk,
            data_chunk,
        }
    }
}
impl fmt::Display for DSFMetadata {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "DSD chunk:\n{}\n\nFmt chunk:\n{}\n\nData chunk:\n{}",
            self.dsd_chunk, self.fmt_chunk, self.data_chunk
        )
    }
}

const DSD_CHUNK_HEADER: [u8; 4] = ['D' as u8, 'S' as u8, 'D' as u8, ' ' as u8];

pub struct DSDChunk {
    pub file_size: u64,
    pub metadata_offset: u64,
}
impl DSDChunk {
    fn new(file_size: u64, metadata_offset: u64) -> DSDChunk {
        DSDChunk {
            file_size,
            metadata_offset,
        }
    }

    fn new_from_buffer(buffer: [u8; 28]) -> Result<DSDChunk, DSFError> {
        if &buffer[0..4] != DSD_CHUNK_HEADER {
            return Err(DSFError::DSDChunkHeader);
        }

        let chunk_size = u64_from_byte_buffer(&buffer, 4);
        if chunk_size != 28 {
            return Err(DSFError::DSDChunkSize);
        }

        let file_size = u64_from_byte_buffer(&buffer, 12);
        let metadata_offset = u64_from_byte_buffer(&buffer, 20);

        Ok(DSDChunk::new(file_size, metadata_offset))
    }
}
impl fmt::Display for DSDChunk {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "File size = {} bytes\nMetadata offset = {} bytes",
            self.file_size, self.metadata_offset
        )
    }
}

const FMT_CHUNK_HEADER: [u8; 4] = ['f' as u8, 'm' as u8, 't' as u8, ' ' as u8];

pub struct FmtChunk {
    pub channel_type: ChannelType,
    pub channel_num: u32,
    pub sampling_frequency: u32,
    pub bits_per_sample: u32,
    pub sample_count: u64,
    pub block_size_per_channel: u32,
}
impl FmtChunk {
    fn new(
        channel_type: ChannelType,
        channel_num: u32,
        sampling_frequency: u32,
        bits_per_sample: u32,
        sample_count: u64,
        block_size_per_channel: u32,
    ) -> FmtChunk {
        FmtChunk {
            channel_type,
            channel_num,
            sampling_frequency,
            bits_per_sample,
            sample_count,
            block_size_per_channel,
        }
    }

    fn new_from_buffer(buffer: [u8; 52]) -> Result<FmtChunk, DSFError> {
        if &buffer[0..4] != FMT_CHUNK_HEADER {
            return Err(DSFError::FmtChunkHeader);
        }

        let chunk_size = u64_from_byte_buffer(&buffer, 4);
        if chunk_size != 52 {
            return Err(DSFError::FmtChunkSize);
        }

        let format_version = u32_from_byte_buffer(&buffer, 12);
        if format_version != 1 {
            return Err(DSFError::FormatVersion);
        }

        let format_id = u32_from_byte_buffer(&buffer, 16);
        if format_id != 0 {
            return Err(DSFError::FormatId);
        }

        let channel_type = ChannelType::new(u32_from_byte_buffer(&buffer, 20))?;
        let channel_num = u32_from_byte_buffer(&buffer, 24);
        let sampling_frequency = u32_from_byte_buffer(&buffer, 28);
        let bits_per_sample = u32_from_byte_buffer(&buffer, 32);
        let sample_count = u64_from_byte_buffer(&buffer, 36);

        let block_size_per_channel = u32_from_byte_buffer(&buffer, 44);
        if block_size_per_channel != BLOCK_SIZE as u32 {
            return Err(DSFError::BlockSizePerChannelNonStandard);
        }

        let reserved = u32_from_byte_buffer(&buffer, 48);
        if reserved != 0 {
            return Err(DSFError::ReservedNotZero);
        }

        Ok(FmtChunk::new(
            channel_type,
            channel_num,
            sampling_frequency,
            bits_per_sample,
            sample_count,
            block_size_per_channel,
        ))
    }

    fn format_duration(&self) -> String {
        let hours = self.sample_count / (self.sampling_frequency as u64 * 3600);
        let minutes = (self.sample_count / (self.sampling_frequency as u64 * 60)) - (hours * 60);
        let seconds =
            (self.sample_count / (self.sampling_frequency as u64)) - (hours * 3600 + minutes * 60);
        let samples = self.sample_count % (self.sampling_frequency as u64);

        format!("{:02}:{:02}:{:02};{}", hours, minutes, seconds, samples)
    }
}
impl fmt::Display for FmtChunk {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Channel type = {}
Channel number = {}
Sampling frequency = {} Hz
Bits per sample = {}
Sample count per channel = {}
Block size per channel = {} bytes
Calculated duration = {} h:min:s;samples",
            self.channel_type,
            self.channel_num,
            self.sampling_frequency,
            self.bits_per_sample,
            self.sample_count,
            self.block_size_per_channel,
            self.format_duration()
        )
    }
}

pub enum ChannelType {
    Mono,
    Stereo,
    ThreeChannels,
    Quad,
    FourChannels,
    FiveChannels,
    FivePointOneChannels,
}
impl ChannelType {
    fn new(channel_type_as_u32: u32) -> Result<ChannelType, DSFError> {
        match channel_type_as_u32 {
            1 => Ok(ChannelType::Mono),
            2 => Ok(ChannelType::Stereo),
            3 => Ok(ChannelType::ThreeChannels),
            4 => Ok(ChannelType::Quad),
            5 => Ok(ChannelType::FourChannels),
            6 => Ok(ChannelType::FiveChannels),
            7 => Ok(ChannelType::FivePointOneChannels),
            _ => Err(DSFError::ChannelType),
        }
    }
}
impl fmt::Display for ChannelType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let channel_type_as_str;

        match self {
            ChannelType::Mono => channel_type_as_str = "Mono",
            ChannelType::Stereo => channel_type_as_str = "Stereo: FL, FR.",
            ChannelType::ThreeChannels => channel_type_as_str = "3 channels: FL, FR, C.",
            ChannelType::Quad => channel_type_as_str = "Quad: FL, FR, BL, BR.",
            ChannelType::FourChannels => channel_type_as_str = "4 channels: FL, FR, C, LFE.",
            ChannelType::FiveChannels => channel_type_as_str = "5 channels: FL, FR, C, BL, BR.",
            ChannelType::FivePointOneChannels => {
                channel_type_as_str = "5.1 channels: FL, FR, C, LFE, BL, BR."
            }
        }

        write!(f, "{}", channel_type_as_str)
    }
}

const DATA_CHUNK_HEADER: [u8; 4] = ['d' as u8, 'a' as u8, 't' as u8, 'a' as u8];

pub struct DataChunk {
    chunk_size: u64,
}
impl DataChunk {
    fn new(chunk_size: u64) -> DataChunk {
        DataChunk { chunk_size }
    }

    fn new_from_buffer(buffer: [u8; 12]) -> Result<DataChunk, DSFError> {
        if &buffer[0..4] != DATA_CHUNK_HEADER {
            return Err(DSFError::DataChunkHeader);
        }

        let chunk_size = u64_from_byte_buffer(&buffer, 4);

        Ok(DataChunk::new(chunk_size))
    }
}
impl fmt::Display for DataChunk {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Data chunk size = {} bytes", self.chunk_size)
    }
}

pub enum DSFError {
    IoError(io::Error),
    DSDChunkHeader,
    DSDChunkSize,
    DSDChunkError,
    FmtChunkHeader,
    FmtChunkSize,
    FormatVersion,
    FormatId,
    ChannelType,
    ChannelNum,
    BlockSizePerChannelNonStandard,
    ReservedNotZero,
    DataChunkHeader,
}
impl From<io::Error> for DSFError {
    fn from(error: io::Error) -> Self {
        DSFError::IoError(error)
    }

    // TODO Display trait?
}

// TODO go through below this line

/*
Nomencleture

DSD Sample Octete is a single byte in the DSD data. It consists of 8 DSD 1-bit samples.

dsopc_total = DSD Sample Octets Per Channel total: the total number of bytes in the DSD sample data per channel.
dsopc_index = DSD Sample Octets Per Channel index: the current index of the byte in the DSD sample data per channel (not per block).
channels_index = The current channel index = 0 to (channels_total - 1). Identifies the current channel we are working with.
channels_total = The number of channels e.g. 2 for stereo, 6 for 5.1 surround sound.
bf_index = Block-Frame index. From 0 to (bf_total - 1). The current block frame in the current_bf Vector.
bf_total = The total number of block-frames ceiling(total blocks / channels_total).
current_bf = A vector which contains the currently loaded block-frame. A vector of blocks, one for each channel of the current DSF frame.
bdso_index = Index of a DSD Sample Octet inside the current block.

*/

const BLOCK_SIZE: usize = 4096;
const DSD_SILENCE_BYTE: u8 = 0x69;
const DSF_SAMPLE_DATA_OFFSET: u64 = 92; //TODO check for off-by-one

pub struct DSFSampleData {
    dsopc_total: u64,
    channel_total: usize,
    channel_index: usize,
    bf_total: u64,
    bf_index: u64,
    bf_size: usize,
    current_bf: Vec<[u8; BLOCK_SIZE]>,
    bdso_index: usize,
    bdso_max_for_last_bf: usize,
    last_bf: bool,
    end_of_data: bool,
    reverse_bits: bool,
    file: File,
}
impl DSFSampleData {
    pub fn new(file: File, metadata: &DSFMetadata) -> Result<DSFSampleData, DSFError> {
        let dsopc_total: u64 = metadata.fmt_chunk.sample_count / 8;

        let channel_total: usize = metadata.fmt_chunk.channel_num as usize;
        let channel_index: usize = 0;

        let dsd_sample_data_size: u64 = metadata.data_chunk.chunk_size - 12;
        let bf_total: u64 = (dsd_sample_data_size / BLOCK_SIZE as u64) / channel_total as u64;
        let bf_index: u64 = 0;
        let bf_size = BLOCK_SIZE * channel_total;

        let mut current_bf: Vec<[u8; BLOCK_SIZE]> = Vec::with_capacity(channel_total as usize);
        for _ in 0..channel_total {
            let block: [u8; BLOCK_SIZE] = [DSD_SILENCE_BYTE; BLOCK_SIZE];
            current_bf.push(block);
        }

        let bdso_index: usize = 0;
        let bdso_max_for_last_bf: usize = (dsopc_total % BLOCK_SIZE as u64) as usize;
        let last_bf: bool = false;
        let end_of_data = false;

        let reverse_bits: bool = metadata.fmt_chunk.bits_per_sample == 1;

        let mut dsf_sample_data: DSFSampleData = DSFSampleData {
            dsopc_total,
            channel_total,
            channel_index,
            bf_total,
            bf_index,
            bf_size,
            current_bf,
            bdso_index,
            bdso_max_for_last_bf,
            last_bf,
            end_of_data,
            reverse_bits,
            file,
        };

        dsf_sample_data.read_bf(0)?;

        Ok(dsf_sample_data)
    }

    pub fn get_fraction(&self) -> f64 {
        (self.bf_index * BLOCK_SIZE as u64 + self.bdso_index as u64) as f64
            / self.dsopc_total as f64
    }

    pub fn get_dsopc_index(&self) -> u64 {
        self.bf_index * BLOCK_SIZE as u64 + self.bdso_index as u64
    }

    pub fn get_dsopc_total(&self) -> u64 {
        self.dsopc_total
    }

    fn read_bf(&mut self, bf_index: u64) -> Result<bool, DSFError> {
        if self.bf_total == 0 || bf_index >= self.bf_total {
            return Ok(false);
        }

        let bf_offset = bf_index * self.bf_size as u64;

        self.file
            .seek(SeekFrom::Start(DSF_SAMPLE_DATA_OFFSET + bf_offset))?;

        for channel_index in 0..self.channel_total {
            let mut block: [u8; BLOCK_SIZE] = [0; BLOCK_SIZE];
            self.file.read_exact(&mut block)?;
            self.current_bf[channel_index] = block;
        }

        self.bf_index = bf_index;
        self.last_bf = self.bf_index + 1 == self.bf_total;
        self.bdso_index = 0;
        self.end_of_data = false;

        Ok(true)
    }

    fn read_next_bf(&mut self) -> Result<bool, DSFError> {
        if self.bf_index + 1 < self.bf_total {
            for channel_index in 0..self.channel_total {
                let mut block: [u8; BLOCK_SIZE] = [0; BLOCK_SIZE];
                self.file.read_exact(&mut block)?;
                self.current_bf[channel_index] = block;
            }

            self.bf_index += 1;
            self.last_bf = self.bf_index + 1 == self.bf_total;
            self.bdso_index = 0;

            return Ok(true);
        } else {
            return Ok(false);
        }
    }

    fn get_dso(&mut self, channel_index: usize, bdso_index: usize) -> u8 {
        if self.last_bf && bdso_index >= self.bdso_max_for_last_bf {
            if self.reverse_bits {
                return DSD_SILENCE_BYTE.reverse_bits();
            } else {
                return DSD_SILENCE_BYTE;
            }
        }

        if self.reverse_bits {
            return self.current_bf[channel_index][bdso_index].reverse_bits();
        } else {
            return self.current_bf[channel_index][bdso_index];
        }
    }

    fn increment_channel_index(&mut self) -> bool {
        if self.channel_index >= self.channel_total - 1 {
            self.channel_index = 0;
            return true;
        } else {
            self.channel_index += 1;
            return false;
        }
    }

    fn increment_bdso_index(&mut self, increment: usize) {
        if self.bdso_index + increment < BLOCK_SIZE {
            self.bdso_index += increment;
        } else {
            match self.read_next_bf() {
                Ok(success) => {
                    if !success {
                        self.end_of_data = true;
                    }
                }
                Err(_) => {
                    println!("ERROR: Failed to read next block-frame.");
                    self.end_of_data = true;
                }
            }
        }

        if self.last_bf && self.bdso_index > self.bdso_max_for_last_bf {
            self.end_of_data = true;
        }
    }
}
impl Iterator for DSFSampleData {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.end_of_data {
            return None;
        }

        let u32be_byte_array: [u8; 4];

        u32be_byte_array = [
            self.get_dso(self.channel_index, self.bdso_index),
            self.get_dso(self.channel_index, self.bdso_index + 1),
            self.get_dso(self.channel_index, self.bdso_index + 2),
            self.get_dso(self.channel_index, self.bdso_index + 3),
        ];

        if self.increment_channel_index() {
            self.increment_bdso_index(4);
        }

        Some(u32::from_le_bytes(u32be_byte_array))
    }
}
impl fmt::Display for DSFSampleData {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Total DSD Sample Octets Per Channel = {}
Number of channels = {}
Number of block-frames = {}
Block-frame size = {} bytes
DSD samples in last block-frame = {}
Reverse bits = {}",
            self.dsopc_total,
            self.channel_total,
            self.bf_total,
            self.bf_size,
            self.bdso_max_for_last_bf,
            self.reverse_bits,
        )
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
